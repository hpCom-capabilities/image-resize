### Setup

##### Prerequisites

1. download [resize-cli](https://bitbucket.org/hpCom-capabilities/image-resize),  
2. go to the repository folder and run  

```
npm install -g
```

##### go to the project folder
```
resize --image="./img/**/*" --size="720,1280"
```

##### paramters

* **image** - [glob](https://github.com/isaacs/node-glob#user-content-glob-primer) pattern to find images
* **size** - image width size

This example find every image from the actual directory and sub directories and creates 2 new file for every image with -720 and -1280 postfix

##### few more examples
every image with **omen-07**, **omen-08**, **omen-09**
```
resize --image="./images/**/*omen-0{7,8,9}*" --size="720,1280"
```

every image with **.png**
```
resize --image="./images/**/*.png" --size="720,1280"
```
