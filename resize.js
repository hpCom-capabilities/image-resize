#!/usr/bin/env node

// CLI example
// resize --image="./img/**/*" --size="720,1280"
// get the images by the image pattern and create 2 file with -720 and -1280 postfix

var argv = require('minimist')(process.argv.slice(2));
var globule = require('globule');
var jimp = require("jimp");
var images;
var pattern; 

function resize(item, size) {
  var newName = item.replace(/\.([a-z,0-9]{3,4})$/i, '-' + size + '.$1');
  jimp.read(item, function (err, img) {
      if (err){
        console.log('error', err);
      }
      img
        .resize(size, jimp.AUTO)
        .write(newName);

      console.log('file created:', newName);
  });
}

if (argv.image && argv.size){
  pattern = [].concat(argv.image);
  images = globule.find(pattern).filter(function (image) {
    //new Regexp('-(' + argv.size + ')')
    var temp = new RegExp('-(' + argv.size.replace(/,/, '|') + ')');
    return !temp.test(image);
  });
  sizes = argv.size.split(',')

  //console.log('arguments:', argv);
  //console.log('pattern', pattern);
  console.log('images:\n', images);
  //console.log('images count:', images.length);
  //console.log('sizes', sizes);

  images.forEach(function (image) {
    sizes.forEach(function (size) {
      //console.log('image', image);
      //console.log('size', size);
      resize(image, size * 1);
    });
  });
}
